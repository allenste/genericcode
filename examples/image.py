#!/usr/bin/env python
"""
.. file:: image.py
.. module:: image
    :synopsis: Contains a class that defines an image object

.. moduleauthor:: Steve Allen <steve.allen@usherbrooke.ca>

"""
from bagay import bagay
import numpy as np

class image(bagay) :
    """This class defines an opbject that contains voxels values. It also
    defines basic operation on images. The images can be distributed on
    many processors. This distribution can take into account an overlap, that
    is images that are repeated on two neighbour process.
    The total number of images is nbimages. The image index goes from 0 to
    nbimages-1.
    Because of the distribution, the array on a process cover just a part of
    these images.
    An overlap of 1 menas that a process knows the last image that belongs to
    each neighbour: proc 0: knows images [0,1,2,3,4], proc 1: knows [3,4,5,6,7]
    But the ownership is: proc 0: [0,1,2,3], proc 1: [4,5,6,7]
    That is for an example of 2 processes with 8 images. Usually only a process
    that own an image should modify it.
    The attribute first corresponds to the index of the 1st image own.

    Attributes
    ----------
    default : dict
        Default values for some of the class attributes

    dtype : `numpy.dtype`
        Type used to store the voxel values

    nbimages : int
        Total number of images (without overlap).

    height : int
        Number of pixels in height

    width : int
        Number of pixels in width

    firstOwn : int
        Index of the first image own by the process

    localFirstOwn : int
        Index of the first image own in the local array.

    nbImageOwn : int
        Number of images own

    nbImageKnown : int
        Number of images known

    overlap : int
        Number of images known by a process that belongs to its neighbour.

    extra : int
        Number of extra images added at each end (padding) to simplify algo

    fill : int or float (same as dtype)
        Value used to fill the extra images.

    distributed : boolean
        Indicates if the images are distributed on many processes or not
    """
    default = {"dtype":np.float32, "nbimages":0, "first":0, "overlap":0, \
        "extra" : 0, "fill" : 0, "distributed" : False}

    def initData(self, fct) :
        """Initialize the pixels with the given function. The function must
        return an ndarray.

        Parameters
        ----------
        self : :class:`image`
            Handle to the object

        fct : callable that returns a numpy.ndarray
        """
        try :
            self.data = fct((self.nbImageKnown,self.height,self.width), \
                self.dtype)
        except :
            print "Error in image.initData"
            print "Unable to build image", self.name, "with", fct
            exit()


    def readData(self, filename) :
        """Read the pixel values from a file.

        Parameters
        ----------
        self : :class:`image`
            Handle to the object

        filename : basestring
            Name of the file where to read the data

        Returns
        -------
        data : numpy.ndarray
            Data read in the file
        """
        return data


    def buildAttributes(self, nbimages) :
        """Using the number of images set the attributes for the object. Take
        into account that the images might need to be distributed.

        Parameters
        ----------
        self : :class:`image`
            Handle to the object

        nbimages : int
            Total number of images that will be stored in the object.
        """
        self.nbimages = nbimages
        try :
            self.mpirank = comm.Get_rank()
            self.mpisize = comm.Get_size()
            self.distributed = True
        except :
            self.mpirank = 0
            self.mpisize = 1
            self.distributed = False

        # Set attributes for the number of images


    def __init__(self, **kwargs) :
        """Build an image. This function setup the additional attributes for
        the given image.

        Parameters
        ----------
        self : :class:`image`
            Handle to the object

        kwargs : dict
            List of attributes used to inialize the object.
        """
        self.loaded = False
        bagay.__init__(self, **kwargs)

        # Initialize attributes if the number of images is set
        if self.nbimages > 0 :
            self.buildAttributes(self.nbimages)

        # Initialize data
        d = vars(self)
        if "initFct" in d : self.initData(self.initFct)
        elif "readFile" in d :
            self.data = self.readData(self.readFile)
            self.loaded = True


    def getNeighbour(self, indx, number, test=None) :
        """Get values of the neighbour of a pixels or of a list of pixels.

        Parameters
        ----------
        self : :class:`image`
            Handle to the object

        indx : int or list of int
            Indices of the pixel for which to return the neighbours

        number : int
            Number of neighbours to look at. Only some possible values 6, 8, 26

        test : callable that returns a boolean
            If present, the function returns only the neighbour that satisfy
            the condition.

        Returns
        -------
        val : numpy.ndarray
            values of the neighbours that meet the test.
        """
        return val


    def getTotalNbImages(self) :
        """Returns the total number of images without overlap and the extra
        images.

        Parameters
        ----------
        self : :class:`image`
            Handle to the object

        Returns
        -------
        nbimages : int
            The total number of images
        """
        return self.nbimages


    def getFirstImageIndex(self) :
        """Returns the index of the first image for the current process.

        Parameters
        ----------
        self : :class:`image`
            Handle to the object

        Returns
        -------
        firstOwn : int
            The index of the first image owned by the process.
        """
        return self.firstOwn


    def getData(self) :
        """Returns the values of the pixels.

        Parameters
        ----------
        self : :class:`image`
            Handle to the object

        Returns
        -------
        data : numpy.ndarray
            The complete array containing the images known.
        """
        return self.data


    def getIndex(self, test, sli=None, absolute=True) :
        """Returns the indices of pixels that meet a given test.

        Parameters
        ----------
        self : :class:`image`
            Handle to the object

        test : A callable that takes an numpy.ndarray and returns a boolean
            Function that test which indices are returned

        sli : tuple of int and/or slice
            To reduce the test to a subset of pixels. The indices returned are
            relative to that subset and not absolute.

        absolute : boolean
            If the index returned ar the abolsute index or relative index for
            process. It cannot be absolute if a sli is given.

        Returns
        -------
        indx : tuple of numpy.ndarray
            Indices of the pixels that meet the test. It is the relative
            position in the subset given by sli.
        """
        if sli == None :
            indx = np.where(test(self.data))
            if absolute : indx[0] += self.first - self.shift
        else : indx = np.where(test(self.data[sli]))
        return indx


    def setValue(self, val, indx, absolute=True) :
        """Set the values of a group of pixels.

        Parameters
        ----------
        self : :class:`image`
            Handle to the object

        val : int or float
            Value that will be given to the voxels at indx

        indx : tuple of numpy.ndarray or int
            Indices of the voxels that will be modified

        absolute : boolean (optional)
            It the indices are given in the absolute coordinates. Default: True
        """


    def setNeighbourValue(self, number, val, test) :
        """Set the neighbour of every pixels satisfying a test to a given value

        Parameters
        ----------
        self : :class:`image`
            Handle to the object

        number : int
            Number of neighbour to change the value. It only works in 2D.

        val : float or int
            Value to give to the neighbour of voxels that meet the condition

        test : callable that takes an numpy.ndarray
            Function used for the test. It must return a numpy.ndarray of
            boolean with same size and shape as the given argument.
        """

