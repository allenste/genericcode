#!/usr/bin/env python
"""
.. file:: resorption.py
.. module:: resorption
    :synopsis: Contains the algorithm to perform the resorption of images

.. moduleauthor:: Steve Allen <steve.allen@usherbrooke.ca>

"""
from algorithm import algorithm
from image import image
import math
import numpy as np

class resorption(algorithm) :
    """This class builds an object that performs resorptions of images. One
    simply needs to define the steps and the method for each step and uses the
    algorithm class for the execution of the algorithm. That latter class
    allows to stop the algorithm where you want, save temporary result and
    restart the algorithm at a given step.

    Attributes
    ----------
    steps : list of dictionaries or algorithms
        Each element gives the name of the method to call, list of inputs and
        outputs. There is also a key to indicate if the step was performed
        succesfully or not.

    step1 : dictionary
        This dictionary describes the first step of the algorithm
    """
    # define the objects that are not build by a step
    fdt = {'name' : 'fdt', 'class' : 'image', 'comm' : True}
    water = {'name' : 'water', 'class' : 'image', 'comm' : True, 'extra' : 1, \
        'fill' : 0, 'overlap' : 1 }
    border = {'name' : 'border', 'class' : 'queue', 'comm' : True}
    pores = {'name' : 'pores', 'class' : 'queue', 'comm' : True}

    # To enable to restart at any steps you need a dictionary to describe the
    # other objects
    region = {'name' : 'region', 'class' : 'image', 'comm' : True, \
        'extra' : 0,'overlap' : 0, 'dtype' : np.uint8}
    cvf = {'name' : 'cvf', 'class' : 'queue', 'comm' : True, "dtype" : np.int64}

    # define each steps of the algorithm
    step1 = {'name' : 'addVoxelsAroundInter', \
             'inputs' : ['water', 'fdt'], \
             'outputs' : ['water']}
    step2 = {'name' : 'initCVF', \
             'inputs' : ['water', 'border'], \
             'outputs' : ['region', 'cvf']}
    step3 = {'name' : 'interconnectLimit'}
    step4 = {'name' : 'resorp', \
             'inputs' : ['water','region','pores','cvf'], \
             'outputs' : ['water','pores','cvf']}
    steps = [step1, step2, step3, step4]


    def __init__(self, **kwargs) :
        """This function initializes an instantiation of a resorption object.
        """
        algorithm.__init__(self, **kwargs)
        if not "res" in vars(self) : self.res = 15


    def addVoxelsAroundInter(self, water, fdt) :
        """This function modifies the value of the voxels around the
        interconnection.

        Parameters
        ----------
        self : :class:`resorption`
            Handle to the object

        water : :class:`image`
            Images obtained by the watershed algorithm. Modified on output.

        fdt : :class:`image` or string or None
            Images obtained by the FDT algorithm. If it is a string, it is the
            name of the file to load. If None, try to load images from FDT.m

        Returns
        -------
        water : :class:`image`
            The watershed image modified.
        """
        # Adding voxels around interconnections to ceramic. It requires an
        # overlap of 1 in the watershed image.
        indx = np.where(np.logical_and(fdt.data<20,fdt.data>=10))
        test = lambda x : x >= 2
        for i in indx :
            neigh = np.unique(water.getNeighbour(i, 26, test))
            if neigh.size > 1 : water.setValue(i,0)

        # Update overlaps
        water.update()

        # Removing some single ceramic voxels among first level pores
        test = lambda x : x == 1
        water.setNeighbourValue(8, test, 1)     # Modification only in 2D

        # Update overlaps
        water.update()

        return water


    def initCVF(self, water, border) :
        """This function creates the initial CVF. In a first step it creates
        the object called region using the border, then using the region and
        the watershed image it builds the initial CVF.

        Parameters
        ----------
        self : :class:`resorption`
            Handle to the object

        water : :class:`image`
            Images obtained by the watershed algorithm. Modified on output.

        border : :class:`queue`
            Queue containing the pixels belonging to the border.

        Returns
        -------
        region : :class:`image`
            Image indicating to what category the pixel belongs to.

        cvf : :class:`queue`
            Volume occupied by the different regions.
        """
        # Evaluate the radius if needed
        if not "radius" in vars(self) :
            self.radius = math.floor(4000./3/self.res)

        # Build the region image
        d = resorption.region
        if "comm" in vars(self) : d["comm"] = self.comm
        else : del d["comm"]
        initFct = lambda d : np.zeros(**d)
        d.update({"image" : water, "initFct" : initFct})
        region = image(**d)

        # Get image index and get a pointer to the data
        excl = int(vars(self).get("exclusion",19))
        first = region.getFirstImageIndex()
        nbimages = region.getTotalNbImages()
        if nbimages < 2*excl+1 :
            print "Not enough images for this algorithm. Reducing exclusion."
            excl = self.exclusion = (nbimages - 1)/2
        data = region.getData()
        begin = max(excl-first,0)
        end = min(data.shape[0]+first,nbimages-excl-1)-first 
        # end is not included in the list

        # Removing part of the code for privacy

        # Count each region to build the original cvf
        vol = np.array((19,),np.int64)
        indx = 0
        test = lambda x : np.where(x[indx] == 0)
        vol[0] = 0
        for i in range(1,19,2) :
            indx = np.where(data==(i+1)/2)
            vol[i] = indx[0].size
            vol[i+1] = water.getIndex(test)[0].size

        # Create the first element in the list of cvf. Will need a reduce
        d = resorption.cvf
        if "comm" in vars(self) : d["comm"] = self.comm
        else : del d["comm"]
        cvf = queue(**d)
        cvf.addElement(vol)
        cvf.reduce(indx=(-1,slice(1,vol.size)))
        return (region, cvf)


    def interconnectLimit(self) :
        """Create the virtual spheres for the interconnection limit. These are
        not returned but keep as an attribute to the algorithm.

        Parameters
        ----------
        self : :class:`resorption`
            Handle to the object
        """
        # Set the limit using threshold and resolution
        self.liml = float(vars(self).get("thl",80))/(2*self.res)
        self.limm = float(vars(self).get("thl",130))/(2*self.res)
        self.limh = float(vars(self).get("thl",180))/(2*self.res)
        self.limlint = int(math.ceil(self.liml))
        self.limmint = int(math.ceil(self.limm))
        self.limhint = int(math.ceil(self.limh))

        # Build the low virtual spheres
        nvox = 2 * self.limlint + 1
        xx, yy, zz = np.mgrid[:nvox, :nvox, :nvox]
        dist = np.sqrt((xx-self.limlint)**2+(yy-self.limlint)**2+ \
            (zz-self.limlint)**2)
        self.virl = np.where(dist <= self.liml)        

        # Build the medium virtual spheres
        nvox = 2 * self.limmint + 1
        xx, yy, zz = np.mgrid[:nvox, :nvox, :nvox]
        dist = np.sqrt((xx-self.limmint)**2+(yy-self.limmint)**2+ \
            (zz-self.limmint)**2)
        self.virm = np.where(dist <= self.limm)        

        # Build the high virtual spheres
        nvox = 2 * self.limhint + 1
        xx, yy, zz = np.mgrid[:nvox, :nvox, :nvox]
        dist = np.sqrt((xx-self.limhint)**2+(yy-self.limhint)**2+ \
            (zz-self.limhint)**2)
        self.virh = np.where(dist <= self.limh)        


    def resorp(self, water, region, pores, cvf) :
        """This function contains the main step of the algorithm. It performs
        the migration and the resportion step by step.

        Parameters
        ----------
        self : :class:`resorption`
            Handle to the object

        water : :class:`image`
            Images obtained by the watershed algorithm. Modified on output.

        region : :class:`image`
            Images defining to whcih category each voxel belongs.

        pores : :class:`queue`
            Queue that contains the list of pores

        cvf : :class:`queue`
            Queue that contains the number of voxels in the different
            categories.

        Returns
        -------
        water : :class:`image`
            The watershed image after resorption.

        pores : :class:`queue`
            The list of pores left.

        cvf : :class:`queue`
            The volume of each regions at each step of the process.
        """
        # if no iterator initializes the iterator
        if not hasattr(self, "iterator") :
            # It also means that the watershed was not adjusted. Adjust it.

            # Adjsut the values for the pore length
            if not "fdtmod" in vars(self) : self.fdtmod = 20

            # Set parameters
            attrib = vars(self)
            if not "resperiod" in attrib : self.resperiod = 3
            if not "colrate" in attrib :
                self.colrate = 4*math.pi*500*500/self.resperiod
            if not "migratel" in attrib : self.migratel = 150
            if not "migratem" in attrib : self.migratem = 350
            if not "migrateh" in attrib : self.migrateh = 650

        return (water,pores,cvf)


def driver(**kwargs) :
    """This function creates an algorithm resorption and run it.

    Parameters
    ----------
    kwargs : dict
        Named arguments required to build the algorithm or the objects
        By default it contains the following two elements:

    args : list of str
        Command line arguments.

    comm : :class:`mpi4py.MPI.Intracomm`
        The Intracommunicator used for the parallel case.
    """
    # Creates the algorithm
    resorp = resorption(**kwargs)

    # Run the algorithm
    res = resorp()


if __name__ == "__main__" :
    import sys
    if len(sys.argv) > 1 : dico = {"args" : sys.argv[1:]}
    else : dico = {}
    try :
        # Run in parallel mode
        from mpi4py import MPI
        dico["comm"] = MPI.COMM_WORLD

    except :
        # Run a serial version
        pass

    driver(**dico)

