#!/usr/bin/env python
"""
.. file:: queue.py
.. module:: queue
    :synopsis: Contains a class that defines a queue object. It stores values
    in a ndarray. Typically the array is 1D or 2D if each column has a similar
    type.

.. moduleauthor:: Steve Allen <steve.allen@usherbrooke.ca>

"""
from bagay import bagay

class queue(bagay) :
    """
    """

