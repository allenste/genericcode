============
Generic Code
============

.. package: genericCode
.. author: Steve Allen, Centre de calcul scientifique, U de Sherbrooke
.. email: steve.allen@usherbrooke.ca

.. brief: This package contains a generic algorithm class and a generic object
    class.
    
The generic object class, bagay, allows to easily configure, store, load an
object for a scientific computation. It can also contain a communicator for
parallel computation purpose.

The algorithm class built on top of the other one, allows to quickly define an
algorithm. It can be executed for a limited number of steps and restarted from
there.

You will find simple examples at the end of each file. More complex examples
can be found in the examples folder. Since the complete code of these examples
corresponds to projects worked on at Universite de Sherbrooke, most of the real
computing part of these codes was removed.


Keywords
--------

Python, MPI, HDF5, Numpy, Json, Pickle

Prerequisites
-------------

Numpy (Lapack, Blas), h5py (HDF5)
Optionally: mpi4py (MPI)

