#!/usr/bin/env python
"""
.. file:: bagay.py
.. module:: bagay
    :synopsis: Contains a generic class that help to define an object, to
    store it, reload it, to configure it, etc.

.. moduleauthor:: Steve Allen <steve.allen@usherbrooke.ca>

.. note:: The word "bagay" (pronounced ba-gay) is from Haitian Creole and means
    "thing". It is commonly used as "Bon bagay" : "It's a good thing".
"""
import re, json, pickle, math, os
import numpy as np

def createObj(desc, **kwargs) :
    """From a dictionary creates automatically a bagay object

    Parameters
    ----------
    desc : dict or basestring
        Description of the object: name (required), class, module, args,
        attributes.

    kwargs : dict
        Dictionary used for the creation of the object.

    Returns
    -------
    The object built.
    """
    if isinstance(desc, dict) :
        # If the description is a dict try to create the object using the dict
        if not 'name' in desc :
            print "The object",desc," does not have a name"
            return None
        name = desc["name"]
        if "module" in desc : module = desc["module"]
        else : module = None
        if "class" in desc : classname = desc["class"]
        else : classname = module
        if classname == None : classname = name
        if module == None : module = classname
        try :
            command = "from "+module+" import "+classname+";"
            if "name" in kwargs :
                command += " obj = "+classname+"(**kwargs)"
            else :
                command += " obj = "+classname+"(name='"+name+"',**kwargs)"
            code = compile(command, "<string>", "exec")
            dico = {"kwargs" : kwargs}
            exec code in dico
        except :
            print "Cannot create object",name
            print "module=",module,"classname=",classname,"command:",command
            print "kwargs=",kwargs
            return None

    elif isinstance(desc, basestring) :
        # If the description is a string try to create the object using it
        name = desc
        try :
            command = "from "+name+" import "+name+";"
            command += "obj = "+name+"(name='"+name+"')"
            code = compile(command, "<string>", "exec")
            dico = {}
            exec code in dico
        except :
            print "Cannot create object",name
            print "Error with command=",command
            return None
    else :
        print "The object",desc,"cannot be used. It must be a string or dict"
        return None
    return dico["obj"]


def combineAttr(d1, d2) :
    """This function updates the first dictionary using the second. But it uses
    the type in the first dictionary to determine the appropriate type.

    Parameters
    ----------
    d1 : dict
        Dictionary to update. Modified on output.

    d2 : dict
        Dictionary whose values will be used to update the other dictionary.
    """
    for k, v in d2.iteritems() :
        if k in d1 :
            if isinstance(d1[k], list) :
                typename = type(d1[k][0]).__name__
                d1[k] = v
                for i in range(len(v)) : d1[k][i] = eval(typename+"(v[i])")
            elif isinstance(d1[k], (int, float)) :
                typename = type(d1[k]).__name__
                d1[k] = eval(typename+"(v)")
            else :
                d1[k] = v
        else : d1[k] = v


def writeSet(fptr, name, array, comm) :
    """This function write a dataset in a HDF5 file in parallel or serial mode.

    Parameters
    ----------
    fptr : :class:`h5py.File`
        Handle to a file or to a group in a file

    name : str
        Name of the dataset to write

    array : :class:`numpy.ndarray`
        Array to be written in the dataset

    comm : :class:`mpi4py.MPI.Intracomm`
        Communicator used for the parallel case
    """
    if comm == None :
        dset = fptr.create_dataset(name, array.shape, array.dtype)
        dset[:] = array
    else :
        mpirank = comm.Get_rank()
        mpisize = comm.Get_size()
        # Parallel case: combine each data together
        buff = np.zeros((len(array.shape),),np.int64)
        comm.Exscan(np.array(array.shape[0],buff.dtype),buff)
        begin = buff[0]
        end = begin + array.shape[0]
        buff[0] = end
        if len(array.shape) > 1 : buff[1:] = array.shape[1:]
        comm.Bcast(buff, root=mpisize-1)
        dset = fptr.create_dataset(name, buff, array.dtype)
        for i in xrange(array.size) :
            dset[i+begin,...] = array[i,...]
        # Add attributes to store the distribution of the data
        for i in range(mpisize) :
            dset.attrs["cpu"+str(i)] = 0
        dset.attrs["cpu"+str(mpirank)] = begin


def writeGroup(fptr, name, array, comm) :
    """This function write a group in a HDF5 file in parallel or serial mode.

    Parameters
    ----------
    fptr : :class:`h5py.File`
        Handle to a file or to a group in a file

    name : str
        Name of the dataset to write

    array : :class:`numpy.ndarray`
        Array to be written in the dataset

    comm : :class:`mpi4py.MPI.Intracomm`
        Communicator used for the parallel case
    """
    if not isinstance(array.flat[0],np.ndarray) : return
    gptr = fptr.create_group(name)
    if comm != None :
        mpirank = comm.Get_rank()
        mpisize = comm.Get_size()
        buff = np.zeros((1,),np.int32)
        comm.Allreduce(np.array([array.size,],buff.dtype),buff)
        nsub = buff[0]
        shape = [0,] * len(array.shape)
        for i in range(mpisize) :
            gptr.attrs["cpu"+str(i)] = shape
        gptr.attrs["cpu"+str(mpirank)] = shape
        if len(array.flat[0].shape) > 1 :
            shape = [0,]+list(array.flat[0].shape[1:])
        else : shape = [0,]
        buff = np.empty(shape,array.flat[0].dtype)
    else :
        nsub = array.size
        gptr.attrs["cpu"+str(0)] = list(array.shape)
    for i in range(nsub) :
        if i < array.size : b = array.flat[i]
        else : b = buff
        if b.dtype == np.dtype('O') : writeGroup(gptr, str(i), b, comm)
        else : writeSet(gptr, str(i), b, comm)


def loadSet(dset, comm) :
    """Function that loads a dataset as an attribute to the object

    Parameters
    ----------
    dset : :class:`h5py.DataSet`
        Handle to a dataset in a HDF5 file

    comm : :class:`mpi4py.MPI.Intracomm`
        Communicator used for the parallel case

    Returns
    -------
    array : :class:`np.ndarray`
        An array containing the dataset.
    """
    if comm != None :
        mpirank = comm.Get_rank()
        mpisize = comm.Get_size()
        # Check if the data was previously distributed the same way
        if len([x for x in dset.attrs.keys() if x.startwith("cpu")])==mpisize :
            begin = dset.attrs.get("cpu"+str(mpirank),0)
            end = dset.attrs.get("cpu"+str(mpirank+1),dset.shape[0])
        # if not distribute it proportionally
        else :
            shape = dset.shape
            nperp = shape[0] / mpisize
            begin = nperp*mpirank + min(mpirank, shape[0]%mpisize)
            end = nperp*(mpirank+1)+min(mpirank+1,shape[0]%mpisize)
        array = np.array(dset[begin:end,...])
    else :
        array = np.array(dset)

    return array


def loadGroup(gptr, comm) :
    """Function that loads a group from an HDF5 file as an attribute to an
    object

    Parameters
    ----------
    gptr : :class:`h5py.Group`
        Handle to a group in a file

    comm : :class:`mpi4py.MPI.Intracomm`
        Communicator used for the parallel case

    Returns
    -------
    array : :class:`np.ndarray`
        An array containing the group.
    """
    if comm != None : mpirank = comm.Get_rank()
    else : mpirank = 0
    shape = gptr.attrs.get("cpu"+str(mpirank),[0,])
    nd = np.prod(shape)
    a = []
    for i, d in enumerate(gptr) :
        if i < nd :
            if isinstance(d,h5py.Dataset) : a.append(loadSet(d, comm))
            else : a.append(loadGroup(d, comm))
    return np.array(a).reshape(shape)


class bagay(object) :
    """This class defines generic methods used to build attributes of an
    object. Those attributes are defined by names, classes, modules.

    Attributes
    ----------
    name: string
        Name for the current instantiation of an object

    filename: string
        Name of the file where to write and read the object. Default is
        name.hdf5

    comm: mpi4py.comm
        MPI communicator
    """

    def parseArguments(self, args, key=None) :
        """This function parse the list of arguments and return the
        interpreted args in a dictionary.

        Parameters
        ----------
        self: :class:`bagay` or derived from it
            Handle to the current object

        args: list of string
            List of arguments passed on the command line.

        key: string starting by "-" and followed by a capitalized letter
            String that enables to get the arguments for this object. If None
            parse the complete list of arguments.

        Returns
        -------
        A dictionary containing the recognized arguments
        """
        d = {}
        s2 = " ".join(args)
        # If there is a capitalized key get the arguments for this object
        if not key == None :
            l = re.split(key, s2, 1)
            if len(l) < 2 : return d
            s1 = l[1]
            s2 = re.split(' -[A-Z]', s1, 1)[0]
        # Get the attributes of the object
        var = vars(self).keys()
        # Look for updated values for each attributes
        for k in var :
            v = re.split(' -'+k+' ', s2, 1)
            if len(v) > 1 : d[k] = re.split(' -[a-z]', v[1], 1)[0].strip()
        return d


    def parseConfigFile(self, **kwargs) :
        """This function parse the list of arguments and return the
        interpreted args in a dictionary.

        Parameters
        ----------
        self: :class:`bagay` or derived from it
            Handle to the current object

        kwargs: dict
            Named arguments. The only one used are:

        configFile: string
            Name of the configuration file. If not present use param.json

        Returns
        -------
        A dictionary containing the recognized arguments
        """
        configFile = kwargs.get("configfile", "params.json")
        d = {"configfile" : configFile}
        # Check that the configuration file can be parsed
        if not isinstance(configFile, basestring) :
            print "Ignoring the configuration file", configFile
            return d
        if not hasattr(self, 'name') :
            print "Cannot parse configuration file for ",self
            print "The object has no attribute called name"
            return d

        # Load the file and get the attributes for this object
        try :
            config = open(configFile)
            data = json.load(config)
        except :
            if "configFile" in kwargs :
                print """Error: The configuration file either does not exist
                    or is not in json format.\nThe file is """,configFile
            return d
        d = data.get(self.name, {})
        if not isinstance(d, dict) :
            print "Error: The configuration of the object",self.name
            print "is not a dictionary. Ignoring it."
            d = {}
        config.close()
        return d


    def load(self, **kwargs) :
        """This function load the values of the attributes from an HDF5 file.

        Parameters
        ----------
        self: :class:`bagay` or derived from it
            Handle to the current object

        kwargs: dict
            Additional named arguments. The following are recognized:

        filename: string
            Name of the configuration file. If None, use self.filename

        comm: :class:`mpi4py.MPI.Comm`
            Communicator to use to load the object. Here are the possibilities
            1) self.comm is not None but kwargs["comm"]==None: each process
               loads the complete ndarrays.
            2) self.comm is None but not kwargs["comm"]: each process loads a
               portion of the ndarrays. Each objects are considered independant
            3) No "comm" in kwargs, use the presence or absence of self.comm
               to determine if the data are distributed.

        loaddata: boolean
            Indicates if the data are loaded or not

        Returns
        -------
        A dictionary containing the loaded attributes
        """
        d = {}
        dico = self.__dict__.copy()

        # Get the filename without extension
        filename = kwargs.get("filename", dico.pop("filename", self.name))
        filename = filename.rpartition(".")[0]

        # Get the communicator
        comm = kwargs.get("comm", dico.get("comm", None))

        # Set the name for the pickle file
        if comm != None :
            n = int(math.log10(comm.Get_size())) + 1
            pickfilename = filename+str(comm.Get_rank()).zfill(n)+".pkl"
        elif dico.get("comm", None) != None :
            n = int(math.log10(self.comm.Get_size())) + 1
            pickfilename = filename+str(self.comm.Get_rank()).zfill(n)+".pkl"
        else : pickfilename = filename+".pkl"

        # Load the parameters from the pickle file
        try :
            f = open(pickfilename, 'r')
            d = pickle.load(f)
            f.close()
        except :
            # There might be no pickle file
            pass

        # Get the list of bagay sub-objects in the current object
        subobjects = d.pop("bagaySubobjects",[])
        localdict = kwargs
        localdict["toload"] = True
        for s in subobjects :
            d[s[0]] = createObj(s[1],**localdict)

        # Set the name of the HDF5 file
        if comm == None and dico.get("comm",None) != None :
            mpirank = comm.Get_rank()
            mpisize = comm.Get_size()
            filename += str(mpirank).zfill(int(math.log10(mpisize))+1)+".hdf5"
        else :
            filename += ".hdf5"

        if not kwargs.get("loaddata",True) : return d

        # If there is an HDF5 file try to load h5py
        if os.path.isfile(filename) :
            try :
                import h5py
            except :
                print "There is an hdf5 file but unable to import h5py."
                print "Ignoring the file."
                return d

            # Open the hdf5 file
            try :
                if comm != None :
                    f = h5py.File(filename, "r", driver="mpio", comm=comm)
                else :
                    f = h5py.File(filename, "r")
            except :
                print filename," is not an HDF5 file. Ignoring."
                return d

            # Load the datasets from the hdf5 file
            for n in f :
                if isinstance(f[n],h5py.Dataset) : d[n] = loadSet(f[n], comm)
                else : d[n] = loadGroup(f[n], comm)

            f.close()

        return d


    def __init__(self, **kwargs) :
        """This function initializes an instance of the class bagay

        Parameters
        ----------
        self: :class:`bagay` or derived from it
            Handle to the current object

        kwargs: dict
            Extra arguments that will be used to build the attributes of the
            object. The bagay class recognizes the following keys:

            filename: string containing the name of the file where to load or
                save the object. Bagay is written to use hdf5 format only.

            name: string containing a given name to the object.

            configfile: File for the configuration (default param.json)

            comm: MPI Intracommunicator for parallel code.

            args: Command line arguments. Can change the values from the
                configuration file.

            key: Name of the key to look for in the args to find the list of
                arguments that apply to this object. If not present, it uses
                the name of the object with the first letter capitalized.

            toload: Boolean that indicates if one want to load the object from
                the .hdf5 and .pkl files (if they are presents). It is the
                default behavior.
        """
        # If the class defines a default list of attributes, initialize with it
        if "default" in dir(self) :
            self.__dict__.update(self.default)

        # If there is a similar object in the arguments init attributes with it
        clname = self.__class__.__name__
        if clname in kwargs :
            self.__dict__.update(vars(kwargs[clname]))

        # Copy arguments to the attributes
        self.name = kwargs.pop("name", clname)
        self.filename = kwargs.pop("filename", self.name+".hdf5")
        self.__dict__.update(kwargs)

        # Build the minimal dictionary required to load the object
        modname = self.__class__.__module__
        self.initdict = {"name" : self.name,"class" : clname,"module":modname}

        # Check if the configFile is modified on the command line
        if "args" in kwargs :
            s2 = " ".join(kwargs["args"])
            v = re.split('-configfile ', s2, 1)
            if len(v) > 1 :
                kwargs["configfile"] = re.split(' -[a-zA-Z]',v[1],1)[0].strip()

        # Look for a configuration file if present parse it
        combineAttr(self.__dict__, self.parseConfigFile(**kwargs))

        # Parse the command line arguments
        args = kwargs.get("args", [])
        if args != [] :
            if 'key' in kwargs : key = kwargs['key']
            else : key = None
            if key == None and hasattr(self,'name') :
                key = '-'+self.name.capitalize()
            combineAttr(self.__dict__, self.parseArguments(args, key))

        # Check that the communicator was properly set
        if (not "comm" in vars(self)) or isinstance(self.comm,bool) :
            self.comm = None

        # Check if the object need to be loaded from a previous execution.
        if vars(self).get("toload",True) : self.__dict__.update(self.load())


    def getCommunicator(self) :
        """Return the Mpi communicator

        Parameters
        ----------
        self: :class:`bagay` or derived from it
            Handle to the current object

        Returns
        -------
        An MPI Intracommunicator or None.
        """
        return self.__dict__.get('comm', None)


    def getName(self) :
        """Return the name of the object

        Parameters
        ----------
        self: :class:`bagay` or derived from it
            Handle to the current object

        Returns
        -------
        A string containing the name of the object
        """
        return self.name


    def getFilename(self) :
        """Return the filename from which to load or where to store the object.

        Parameters
        ----------
        self: :class:`bagay` or derived from it
            Handle to the current object

        Returns
        -------
        A string containing the filename.
        """
        return self.filename


    def store(self, **kwargs) :
        """This function store the values of the attributes in an HDF5 file.
        The user can define is own function to write the data. This function is
        intended for checkpoint purpose. One need to write every attributes to
        be able to regenerate the object automatically.

        Parameters
        ----------
        self: :class:`bagay` or derived from it
            Handle to the current object

        kwargs: dict
            Additional named arguments. The following are recognized:

        filename: string
            Name of the configuration file. If None, use self.filename

        comm: :class:`mpi4py.MPI.Comm`
            To use a different communicator than self.comm when storing the
            file. If your code is parallel and the object is not aware of it
            you might need to give the communicator. If you want a separate
            file for each process give comm=None.

        root : int
            When each process have a copy of the same object, only the root
            will store the data.

        storedata : boolean
            If the data are to be strored in a file or only the other attributes

        ..notes:: There are 3 possible situations concerning the communicator
        1) None in the arguments, use the one in the attributes
        2) One in arguments but none in attributes: Each process have their
            own objects. Write them together in a same file, with a diff name.
        3) One in arguments and one in attribute. Useful for each writing their
           own file. Adjust the filename if not given
        """
        # Initializes a dictionary for non iterable attributes (small)
        params = {}

        # Get a dictionary of attributes
        dico = vars(self)

        # Get the filename
        filename = kwargs.get("filename", dico.pop("filename", self.name))

        # Build the filename for pickle
        pickfilename = filename.rpartition(".")[0]

        # Set the Mpi communicator used for writing the file
        if not "comm" in kwargs :
            # No comm given. Use the one defined in the object
            comm = dico.pop("comm", None)

        elif dico.pop("comm", None) != None :
            # Use a different communicator than the one in attributes
            # Usually it is for each process to write in is own file
            # It can also be because not every process can access the same
            # storage place.
            comm = kwargs["comm"]
            # If no filename was given make sure that each process uses a
            # different filename.
            if (comm == None and (not "filename" in kwargs)) :
                l = filename.rpartition(".")
                try :
                    rank = self.comm.Get_rank()
                    n = int(math.log10(self.comm.Get_size())) + 1
                    filename = l[0]+str(rank+1).zfill(n)+"."+l[2]
                except :
                    pass

        else :
            # A communicator is given in the arguments.
            # The object is not aware of the parallelization. Each process has
            # is own independant object or they each have a copy of the same.
            comm = kwargs["comm"]

        storedata = kwargs.get("storedata",True)

        # Save the number of processes among the attributes
        if comm != None :
            try :
                params["mpisize"] = comm.Get_size()
            except :
                print "Warning: The communicator cannot be used to write", \
                    self.name, " Ignoring.\n"
                comm = None

        # Check if any Numpy array in the object
        arrayInside = any(isinstance(v, np.ndarray) for v in dico.values())

        if arrayInside :
            # Try to load h5py
            try :
                import h5py
            except :
                print "Error: Unable to store",self.name
                print "H5PY is not available."
                return

            # Open the file
            if comm != None :
                # Parallel writing
                f = h5py.File(filename, "w", driver="mpio", comm=comm)
            else :
                # Serial writing
                f = h5py.File(filename, "w")

        # Loop over every attributes
        for k, a in dico.iteritems() :
            if isinstance(a, bagay) :
                # If it is a bagay object write in a separate file
                a.store(**kwargs)
                if not "bagaySubObjects" in params :
                    params["bagaySubobjects"] = (k,a.initdict)
                else : params["bagaySubobjects"].append((k,a.initdict))

            elif isinstance(a, np.ndarray) :
                if not storedata : continue
                # if it is an ndarray write in a dataset
                if a.dtype == np.dtype('O') : writeGroup(f, k, a, comm)
                else : writeSet(f, k, a, comm)

            elif k != "filename"  and k != "name" and not hasattr(a,'__call__'):
                # if it is scalar add to the params
                params[k] = a

        # Close the file
        if arrayInside :
            f.close()

        if len(params) == 0 : return

        # For the small parameters, each process use pickle to save them
        if self.__dict__.get("comm", None) != None :
            mpisize = self.comm.Get_size()
            mpirank = self.comm.Get_rank()
        elif comm != None : mpisize = comm.Get_size()
        else : mpisize = 1
        if mpisize > 1 :
            pickfilename += str(mpirank).zfill(int(math.log10(mpisize))+1)
        f = open(pickfilename+".pkl", 'w')
        pickle.dump(params, f)
        f.close()


def test() :
    """Build a bagay instantiation, store it and reload it.
    """
    name = "myobj"

    # Create a first object and store it
    attr = {"dim":3, "xdim":10, "ydim":12, "zdim":13}
    bag = bagay(name=name,**attr)
    bag.data = np.random.rand(bag.xdim, bag.ydim)
    bag.store()

    # Create a second object from loading the file
    bag2 = bagay(name=name)
    d = bag2.load()
    bag2.__dict__.update(d)

    # Compare the two objects
    print "Difference in parameters:",bag.xdim-bag2.xdim,bag.ydim-bag2.ydim, \
        bag.zdim-bag2.zdim
    eps = 1.e-8
    print "Difference in data\n",np.where(np.absolute(bag.data-bag2.data)>eps)


if __name__ == "__main__" :
    # Run a test
    test()

