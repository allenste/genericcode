#!/usr/bin/env python
"""
.. file:: algorithm.py
.. module:: algorithm
    :synopsis: Contains a generic class that help to define an algorithm.

.. moduleauthor:: Steve Allen <steve.allen@usherbrooke.ca>

"""
from bagay import bagay, createObj
import itertools


def inputToInit(inputlist, outputlist) :
    """This function generates a list of inputs that are not produced by
    previous steps and put them in a list of inputs to be initialized.

    Parameters
    ----------
    inputlist: list of list of strings
        List of input names for each steps of the algorithm.

    outputlist: list of list of strings
        List of output names produced by each steps of the algorithm.

    Returns
    -------
    A list of list containing the names of inputs that need to be initialized
    before each steps of the algorithm.
    """
    inputs = [inputlist[0],]
    for i in range(1, len(inputlist)) :
        names = []
        for n in inputlist[i] :
            # Check if it is in the previous list of inputs
            if n in list(itertools.chain.from_iterable(inputlist[:i])) :
                continue

            # Check if it is in the previous list of outputs
            if n in list(itertools.chain.from_iterable(outputlist[:i])) :
                continue

            # If it is not in the list of previous inputs and outputs, add it
            names.append(n)

        # Put the list for the current step in the input list
        inputs.append(names)

    return inputs


def outputToSave(outputlist) :
    """This function generates a list of outputs that need to be saved because
    they won't be modified any more.

    Parameters
    ----------
    outputlist: list of list of strings
        List of output names produced by each steps of the algorithm.

    Returns
    -------
    A list of list containing the names of outputs that need to be saved
    after each steps of the algorithm.
    """
    outputs = []
    for i in range(len(outputlist)-1) :
        names = []
        for n in outputlist[i] :
            # Check that n is not modified by another step
            if n in list(itertools.chain.from_iterable(outputlist[i+1:])) :
                continue

            # Add to the list of output to save
            names.append(n)

        # Put the list of output to save in the list
        outputs.append(names)

    # Add the outputs of the last step
    outputs.append(outputlist[-1])

    return outputs


def outputToGarbage(inputlist, outputlist) :
    """This function generates a list of outputs that can be garbaged after
    each steps of the algorithm.

    Parameters
    ----------
    inputlist: list of list of strings
        List of input names for each steps of the algorithm.

    outputlist: list of list of strings
        List of output names produced by each steps of the algorithm.

    Returns
    -------
    A list of list containing the names of outputs that can be garbaged after
    each steps.
    """
    outputs = []
    for i in range(len(outputlist)-1) :
        names = []
        for n in outputlist[i] :
            # Check that n is not required as inputs on a next step
            if n in list(itertools.chain.from_iterable(inputlist[i+1:])) :
                continue

            # Add to the list of output to garbage
            names.append(n)

        # Put the list of output for the current step in the list
        outputs.append(names)

    # Add an empty list at the end, sicne the algorithm returns the outputs of
    # the last step.
    outputs.append([])

    return outputs


def inputToLoad(initObj, prevoutputs) :
    """This function builds a list of objects that need to be loaded from a
    previous execution.

    Parameters
    ----------
    initObj : list of list of strings
        List of object to initialize at each step of the algorithm

    prevoutputs : list of list of strings
        List of outputs that were produced at each step of a previous execution

    Returns
    -------
    toload : list of list of strings
        List of object names that need to be loaded at each step of the
        algorithm
    """
    toload = []
    flat = [item for sublist in prevoutputs for item in sublist]
    # Iterate on each algorithm step
    for i in initObj :
        # Check for each object to initialize if it was modified by a previous
        # execution.
        obj = [x for x in i if x in flat]
        toload.append(obj)
    return toload


class algorithm(bagay) :
    """This class is a generic class that manage the pipeline of data for an
    algorithm and allows to stop and restart at a given step. It is base on the
    more general class bagay.
    """
    def __init__(self, **kwargs) :
        """Initialize the algorithm described by the steps. It adjust the
        algorithm to the desired steps.

        Parameters
        ----------
        self : :class:`algorithm` or derived from it
            Instantiation of an algorithm or a daugther class

        kwargs: dict
            Dictionary of attributes to build the algorithm. For a parallel
            algorithm use the key "comm" to pass an MPI communicator.
        """
        # Make sure that previous execution results are not loaded by default
        self.toload = False

        # By default the algorithm does not verbose step execution
        self.verbose = False

        # Call the general initialization
        bagay.__init__(self, **kwargs)

        # Set the list of steps to execute
        try :
            start = int(self.__dict__.get('start', 0))
            stop = int(self.__dict__.get('stop', len(self.totsteps)))
            if start > 0 :
                self.__dict__.update(self.load())
                prevsteps = self.totsteps[:start]
            else : prevsteps = []
            self.steps = self.totsteps[start:stop]
        except :
            print "Error: unable to set the start and stop for algorith"
            print self.name,"\nAcceptable values are 0<start<stop<="
            print len(self.totsteps)
            exit()

        # Generate the total list of inputs and outputs for the algorithm
        inputs = [s.get('inputs',[]) for s in self.steps]
        outputs = [s.get('outputs',[]) for s in self.steps]
        if len(prevsteps) > 0 :
            prevoutputs = [s.get('outputs',[]) for s in prevsteps]
        else : prevoutputs = []

        # Generate the list of inputs that need to be initialized
        self.initObj = inputToInit(inputs, outputs)

        # Generate the list of outputs that need to be saved and garbaged
        self.saveObj = outputToSave(outputs)
        self.garbageObj = outputToGarbage(inputs, outputs)

        # Indicate for each object if it needs to be loaded from a previous
        # execution
        self.objToLoad = inputToLoad(self.initObj, prevoutputs)


    def __call__(self, **kwargs) :
        """Execute the algorithm described by the steps

        Parameters
        ----------
        self : :class:`algorithm` or derived from it
            Instantiation of an algorithm or a daugther class

        kwargs: dict
            Dictionary of inputs required for the execution of the algorithm

        Returns
        -------
        A tuple containing the outputs of the last step of the algorithm.
        """
        objs = kwargs
        objs["algo"] = self
        for ns in range(len(self.steps)) :
            s = self.steps[ns]
            # Check if the step is already completed
            if s.get("status", None) == "completed" : continue

            # Check if the step can be executed
            if not "name" in s :
                print "Step",s['name'],"has no name. Stop"
                return None

            # Add outputs if any
            if "outputs" in s :
                outputs = s["outputs"]
                if hasattr(outputs, "__iter__") :
                    command = ",".join(outputs) + " = "
                elif isinstance(outputs,basestring) : command = outputs + " = "
            else : command = ""

            # Add call to the step
            command += "algo."+s["name"]+"("

            # Add inputs if any required
            if 'inputs' in s :
                inputs = s['inputs']
                if hasattr(inputs, "__iter__") :
                    command += ",".join(inputs)
                elif isinstance(inputs, basestring) : command += inputs
            command += ")"

            # Initialize the objects that were not in kwargs
            for i in self.initObj[ns] :
                if not i in kwargs and i in dir(self) :
                    # Create the input using the given dictionary
                    desc = eval("self."+i)
                    # Check if it needs to be loaded
                    if i in self.objToLoad[ns] : desc["toload"] = True
                    else : desc["toload"] = False
                    initargs = desc
                    if "configfile" in vars(self) :
                        initargs["configfile"] = self.configfile
                    if "args" in vars(self) : initargs["args"] = self.args
                    if desc.get("comm", False) :
                        initargs["comm"] = self.__dict__.get("comm",None)
                    if "attr" in desc : initargs.update(desc["attr"])
                    objs[i] = createObj(desc, **initargs)

            # Execute the step
            code = compile(command, '<string>', 'exec')
            try :
                exec code in objs
                del objs["__builtins__"]
            except :
                del objs["__builtins__"]
                print "Unable to execute step",s["name"]
                print "command=", command, "objs=", objs
                print "Stopping program"
                s['status'] = "Error"
                return

            # Write the step as completed
            s["status"] = "completed"
            self.store()

            # Save the objects that won't be modified
            for o in self.saveObj[ns] : objs[o].store()

            # Run the garbage collector to save memory
            for o in self.garbageObj[ns] : del objs[o]

        # Return the output of the last step executed
        if "outputs" in s : res = tuple([objs[x] for x in outputs])
        else : res = tuple()
        return res


def test() :
    """Run a test: Define an algorithm and run it.
    """
    # Define the algorithm
    class testalg(algorithm) :
        myobj1 = {'name':'myobj1', 'class' : 'bagay', 'args':["v1=1",]}
        s1 = {'name':'f1', 'inputs':['myobj1',], 'outputs':['myobj1',]}
        s2 = {'name':'f2', 'inputs':['myobj1',], 'outputs':['myobj2',]}
        steps = [s1, s2]
        def f1(self, obj1) :
            obj1.v1 = 2
            print "In f1",obj1.v1
            return obj1
        def f2(self, obj1) :
            obj2 = bagay(name="myobj2")
            obj2.v2 = obj1.v1
            print "In f2, obj2.v2=",obj2.v2
            return obj2

    # Build an instantiation of the algorithm
    talg = testalg()

    # Run the algorithm        
    res = talg()

    # Store the algorithm
    talg.store()
    # Print the result
    try :
        print res[0].v2
    except:
        print "The final output was not properly created."


if __name__ == "__main__" :
    test()

